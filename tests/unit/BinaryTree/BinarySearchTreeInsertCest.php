<?php

declare(strict_types=1);

namespace Tests\Unit\BinaryTree;

use Mockery;
use Tests\UnitTester;
use Trees\BinaryTree\BinarySearchTree;
use Trees\BinaryTree\BinaryTreeNode;

/**
 * Class BinarySearchTreeInsertCest
 * @package Tests\Unit\BinaryTree
 */
class BinarySearchTreeInsertCest
{
    /** @var BinarySearchTree|Mockery\Mock */
    private $tree;

    /** @var BinaryTreeNode|Mockery\Mock */
    private $node;

    /** @var BinaryTreeNode|Mockery\Mock */
    private $rightNode;

    /** @var BinaryTreeNode|Mockery\Mock */
    private $rightRightNode;

    /** @var BinaryTreeNode|Mockery\Mock */
    private $leftNode;

    /** @var BinaryTreeNode|Mockery\Mock */
    private $leftLeftNode;

    /**
     * @param UnitTester $I
     */
    public function _before(UnitTester $I)
    {
        $this->node = Mockery::mock(BinaryTreeNode::class);
        $this->tree = Mockery::mock(BinarySearchTree::class, [10])->makePartial();
        $this->tree->shouldReceive('getRoot')->byDefault()->andReturn($this->node);
        $this->rightNode = Mockery::mock(BinaryTreeNode::class);
        $this->rightRightNode = Mockery::mock(BinaryTreeNode::class);
        $this->leftNode = Mockery::mock(BinaryTreeNode::class);
        $this->leftLeftNode = Mockery::mock(BinaryTreeNode::class);
    }

    /**
     * @param UnitTester $I
     */
    public function _after(UnitTester $I)
    {
        Mockery::close();
    }

    /**
     * @param UnitTester $I
     * @throws \Trees\BinaryTree\Exceptions\BinaryTreeNullValueException
     */
    public function newValueInsertedGreaterThanNodeValueButRightNodeExists(UnitTester $I)
    {
        $searchValue = 5;
        $this->tree->shouldReceive('getRoot')->andReturn($this->node);
        $this->node->shouldReceive('isLessThan')->once()->andReturnTrue();
        $this->node->shouldReceive('getRight')->twice()->andReturn($this->rightNode);
        $this->rightNode->shouldReceive('isLessThan')->once()->andReturnTrue();
        $this->rightNode->shouldReceive('getRight')->once()->andReturnNull();
        $this->rightNode->shouldReceive('setRight')->once()->with($searchValue)->andReturn($this->rightRightNode);
        $this->rightRightNode->shouldReceive('setParent')->with($this->rightNode)->once()->andReturnSelf();
        /** @var BinaryTreeNode $response */
        $response = $this->tree->insert($searchValue);
        $I->assertSame(
            $this->rightRightNode,
            $response,
            "rightRightNode should be expected as the return of a new tree node"
        );
    }

    /**
     * @param UnitTester $I
     * @throws \Trees\BinaryTree\Exceptions\BinaryTreeNullValueException
     */
    public function newValueInsertedLessThanNodeValueButLeftNodeExists(UnitTester $I)
    {
        $searchValue = 5;
        $this->tree->shouldReceive('getRoot')->andReturn($this->node);
        $this->node->shouldReceive('isLessThan')->once()->andReturnFalse();
        $this->node->shouldReceive('isGreaterThan')->once()->andReturnTrue();
        $this->node->shouldReceive('getLeft')->twice()->andReturn($this->leftNode);
        $this->leftNode->shouldReceive('isLessThan')->once()->andReturnFalse();
        $this->leftNode->shouldReceive('isGreaterThan')->once()->andReturnTrue();
        $this->leftNode->shouldReceive('getLeft')->once()->andReturnNull();
        $this->leftNode->shouldReceive('setLeft')->once()->with($searchValue)->andReturn($this->leftLeftNode);
        $this->leftLeftNode->shouldReceive('setParent')->with($this->leftNode)->once()->andReturnSelf();
        /** @var BinaryTreeNode $response */
        $response = $this->tree->insert($searchValue);
        $I->assertSame(
            $this->leftLeftNode,
            $response,
            "leftLeftNode should be expected as the return of a new tree node"
        );
    }

    /**
     * @param UnitTester $I
     * @throws \Trees\BinaryTree\Exceptions\BinaryTreeNullValueException
     */
    public function newValueNotInsertedBecauseItExistsAlready(UnitTester $I)
    {
        $searchValue = 5;
        $this->tree->shouldReceive('getRoot')->andReturn($this->node);
        $this->node->shouldReceive('isLessThan')->once()->andReturnFalse();
        $this->node->shouldReceive('isGreaterThan')->once()->andReturnTrue();
        $this->node->shouldReceive('getLeft')->twice()->andReturn($this->leftNode);
        $this->leftNode->shouldReceive('isLessThan')->once()->andReturnFalse();
        $this->leftNode->shouldReceive('isGreaterThan')->once()->andReturnFalse();
        /** @var BinaryTreeNode $response */
        $response = $this->tree->insert($searchValue);
        $I->assertSame(
            $this->leftNode,
            $response,
            "leftNode should be expected as the return of a new tree node because logic indicates the search value is that of this node's"
        );
    }
}
