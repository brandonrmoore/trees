<?php

declare(strict_types=1);

namespace Tests\Unit\BinaryTree;

use Mockery;
use Tests\UnitTester;
use Trees\BinaryTree\BinarySearchTree;
use Trees\BinaryTree\BinaryTreeNode;

/**
 * Class BinarySearchTreeSearchCest
 * @package Tests\Unit\BinaryTree
 */
class BinarySearchTreeSearchCest
{
    /** @var BinarySearchTree|Mockery\Mock */
    private $tree;

    /** @var BinaryTreeNode|Mockery\Mock */
    private $node;

    /** @var BinaryTreeNode|Mockery\Mock */
    private $rightNode;

    /** @var BinaryTreeNode|Mockery\Mock */
    private $leftNode;

    /**
     * @param UnitTester $I
     */
    public function _before(UnitTester $I)
    {
        $this->node = Mockery::mock(BinaryTreeNode::class);
        $this->tree = Mockery::mock(BinarySearchTree::class, [10])->makePartial();
        $this->tree->shouldReceive('getRoot')->byDefault()->andReturn($this->node);
        $this->rightNode = Mockery::mock(BinaryTreeNode::class);
        $this->leftNode = Mockery::mock(BinaryTreeNode::class);
    }

    public function _after(UnitTester $I)
    {
        Mockery::close();
    }

    /**
     * @param UnitTester $I
     */
    public function searchToTheLeft(UnitTester $I)
    {
        $searchValue = 5;
        $this->leftNode->shouldReceive('isLessThan')->with($searchValue)->once()->andReturnFalse();
        $this->leftNode->shouldReceive('isGreaterThan')->with($searchValue)->once()->andReturnFalse();
        $this->node->shouldReceive('isLessThan')->with($searchValue)->once()->andReturnFalse();
        $this->node->shouldReceive('isGreaterThan')->with($searchValue)->once()->andReturnTrue();
        $this->node->shouldReceive('getRight')->never();
        $this->node->shouldReceive('getLeft')->once()->andReturn($this->leftNode);
        $this->tree->shouldReceive('getRoot')->andReturn($this->node);
        /** @var BinaryTreeNode $response */
        $response = $this->tree->search($searchValue);
        $I->assertSame($this->leftNode, $response, "Returned value is not the expected left node");
    }

    /**
     * @param UnitTester $I
     */
    public function searchToTheRight(UnitTester $I)
    {
        $searchValue = 5;
        $this->rightNode->shouldReceive('isLessThan')->with($searchValue)->once()->andReturnFalse();
        $this->rightNode->shouldReceive('isGreaterThan')->with($searchValue)->once()->andReturnFalse();
        $this->node->shouldReceive('isLessThan')->with($searchValue)->once()->andReturnTrue();
        $this->node->shouldReceive('isGreaterThan')->with($searchValue)->never()->andReturnFalse();
        $this->node->shouldReceive('getLeft')->never();
        $this->node->shouldReceive('getRight')->once()->andReturn($this->rightNode);
        $this->tree->shouldReceive('getRoot')->andReturn($this->node);
        /** @var BinaryTreeNode $response */
        $response = $this->tree->search($searchValue);
        $I->assertSame($this->rightNode, $response, "Returned value is not the expected left node");
    }
}
