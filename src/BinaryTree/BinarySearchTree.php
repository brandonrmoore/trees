<?php

declare(strict_types=1);

namespace Trees\BinaryTree;

use Trees\BinaryTree\Interfaces\BinarySearchTreeInterface;
use Trees\BinaryTree\Interfaces\BinaryTreeNodeInterface;

/**
 * Class BinarySearchTree
 * @package Trees\BinaryTree
 */
class BinarySearchTree implements BinarySearchTreeInterface
{
    /**
     * @var BinaryTreeNode|null
     */
    private $root = null;

    /**
     * BinarySearchTree constructor.
     * @param null $value
     * @throws Exceptions\BinaryTreeNullValueException
     */
    public function __construct($value = null)
    {
        $this->root = new BinaryTreeNode($value);
    }

    /**
     * @param $value
     * @return BinaryTreeNodeInterface|null
     */
    public function search($value): ?BinaryTreeNodeInterface
    {
        $node = $this->getRoot();

        while ($node) {
            if ($node->isLessThan($value)) {
                $node = $node->getRight();
            } elseif ($node->isGreaterThan($value)) {
                $node = $node->getLeft();
            } else {
                break;
            }
        }

        return $node;
    }

    /**
     * @param $value
     * @return BinaryTreeNodeInterface|null
     * @throws Exceptions\BinaryTreeNullValueException
     */
    public function insert($value): ?BinaryTreeNodeInterface
    {
        $node = $this->getRoot();

        while (true) {
            if ($node->isLessThan($value)) {
                if ($node->getRight() !== null) {
                    $node = $node->getRight();
                } else {
                    $node = $node->setRight($value)->setParent($node);
                    break;
                }
            } elseif ($node->isGreaterThan($value)) {
                if ($node->getLeft() !== null) {
                    $node = $node->getLeft();
                } else {
                    $node = $node->setLeft($value)->setParent($node);
                    break;
                }
            } else {
                break;
            }
        }
        return $node;
    }

    /**
     * @param mixed $value
     * @return BinarySearchTreeInterface
     * @throws Exceptions\BinaryTreeNullValueException
     */
    public function delete($value): BinarySearchTreeInterface
    {
        $node = $this->search($value);
        if ($node) {
            $node->delete();
        }
        return $this;
    }

    /**
     * @param BinaryTreeNodeInterface|null $node
     * @param array $walkedTree
     * @return array|bool
     */
    public function walk(BinaryTreeNodeInterface $node = null, array $walkedTree = []): array
    {
        if (!$node) {
            $node = $this->getRoot();
        }
        if (!$node) {
            return [];
        }
        if ($node->getLeft()) {
            $walkedTree = $this->walk($node->getLeft(), $walkedTree);
        }
        $walkedTree[] = $node;
        if ($node->getRight()) {
            $walkedTree = $this->walk($node->getRight(), $walkedTree);
        }
        return $walkedTree;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_map(
            function (BinaryTreeNode $node) {
                return $node->getValue();
            },
            $this->walk()
        );
    }

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getMin(): BinaryTreeNodeInterface
    {
        return $this->getRoot()->getMin();
    }

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getMax(): BinaryTreeNodeInterface
    {
        return $this->getRoot()->getMax();
    }

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getRoot(): BinaryTreeNodeInterface
    {
        return $this->root;
    }
}
