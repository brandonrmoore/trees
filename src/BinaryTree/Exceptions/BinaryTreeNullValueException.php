<?php

declare(strict_types=1);

namespace Trees\BinaryTree\Exceptions;

class BinaryTreeNullValueException extends \Exception
{

}
