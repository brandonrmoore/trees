<?php

declare(strict_types=1);

namespace Trees\BinaryTree\Interfaces;

use Trees\BinaryTree\Exceptions\BinaryTreeNullValueException;

interface BinarySearchTreeInterface
{
    /**
     * @param $value
     * @return BinaryTreeNodeInterface|null
     */
    public function search($value): ?BinaryTreeNodeInterface;

    /**
     * @param $value
     * @return BinaryTreeNodeInterface|null
     * @throws BinaryTreeNullValueException
     */
    public function insert($value): ?BinaryTreeNodeInterface;

    /**
     * @param mixed $value
     * @return BinarySearchTreeInterface
     */
    public function delete($value): BinarySearchTreeInterface;

    /**
     * @param BinaryTreeNodeInterface|null $node
     * @param array $walkedTree
     * @return array|bool
     */
    public function walk(BinaryTreeNodeInterface $node = null, array $walkedTree = []): array;

    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getMin(): BinaryTreeNodeInterface;

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getMax(): BinaryTreeNodeInterface;

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getRoot(): BinaryTreeNodeInterface;
}
