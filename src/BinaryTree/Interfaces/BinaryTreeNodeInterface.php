<?php

declare(strict_types=1);

namespace Trees\BinaryTree\Interfaces;

use Trees\BinaryTree\Exceptions\BinaryTreeNullValueException;

/**
 * Interface BinaryTreeNodeInterface
 * @package Trees\BinaryTree\Interfaces
 */
interface BinaryTreeNodeInterface
{
    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @return BinaryTreeNodeInterface|null
     */
    public function getLeft(): ?BinaryTreeNodeInterface;

    /**
     * @param $value
     * @return BinaryTreeNodeInterface
     */
    public function setLeft($value): BinaryTreeNodeInterface;

    /**
     * @return BinaryTreeNodeInterface|null
     */
    public function getRight(): ?BinaryTreeNodeInterface;

    /**
     * @param $value
     * @return BinaryTreeNodeInterface
     * @throws BinaryTreeNullValueException
     */
    public function setRight($value): BinaryTreeNodeInterface;

    /**
     * @return BinaryTreeNodeInterface|null
     */
    public function getParent(): ?BinaryTreeNodeInterface;

    /**
     * @param BinaryTreeNodeInterface|null $parent
     * @return BinaryTreeNodeInterface
     */
    public function setParent(?BinaryTreeNodeInterface $parent): BinaryTreeNodeInterface;

    /**
     * @param $value
     * @return bool
     */
    public function isGreaterThan($value);

    /**
     * @param $value
     * @return bool
     */
    public function isLessThan($value);

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getMin(): BinaryTreeNodeInterface;

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getMax(): BinaryTreeNodeInterface;

    /**
     * @return bool
     */
    public function isRightNodeOfParent(): bool;

    /**
     * @return bool
     */
    public function isLeftNodeOfParent(): bool;

    /**
     * @return void
     * @throws BinaryTreeNullValueException
     */
    public function delete(): void;
}
