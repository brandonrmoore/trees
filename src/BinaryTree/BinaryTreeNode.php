<?php

declare(strict_types=1);

namespace Trees\BinaryTree;

use Trees\BinaryTree\Exceptions\BinaryTreeNullValueException;
use Trees\BinaryTree\Interfaces\BinaryTreeNodeInterface;

/**
 * Class BinaryTreeNode
 * @package Trees\BinaryTree
 */
class BinaryTreeNode implements BinaryTreeNodeInterface
{
    private $value = null;
    private $parent = null;
    private $left = null;
    private $right = null;

    /**
     * BinaryTreeNode constructor.
     * @param $value
     * @param BinaryTreeNode|null $parent
     * @throws BinaryTreeNullValueException
     */
    public function __construct($value, ?BinaryTreeNode $parent = null)
    {
        if ($value === null) {
            throw new BinaryTreeNullValueException('Null values are not acceptable node values');
        }
        $this->value = $value;
        $this->parent = $parent;
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return BinaryTreeNodeInterface|null
     */
    public function getLeft(): ?BinaryTreeNodeInterface
    {
        return $this->left;
    }

    /**
     * @param $value
     * @return BinaryTreeNode
     * @throws BinaryTreeNullValueException
     */
    public function setLeft($value): self
    {
        if ($value instanceof BinaryTreeNodeInterface) {
            $this->left = $value;
        } else {
            $this->left = new self($value);
        }
        return $this;
    }

    /**
     * @return BinaryTreeNodeInterface|null
     */
    public function getRight(): ?BinaryTreeNodeInterface
    {
        return $this->right;
    }

    /**
     * @param $value
     * @return BinaryTreeNodeInterface
     * @throws BinaryTreeNullValueException
     */
    public function setRight($value): self
    {
        if ($value instanceof BinaryTreeNodeInterface) {
            $this->right = $value;
        } else {
            $this->right = new self($value);
        }
        return $this;
    }

    /**
     * @return BinaryTreeNode|null
     */
    public function getParent(): ?BinaryTreeNodeInterface
    {
        return $this->parent;
    }

    /**
     * @param BinaryTreeNodeInterface|null $parent
     * @return BinaryTreeNodeInterface
     */
    public function setParent(?BinaryTreeNodeInterface $parent): BinaryTreeNodeInterface
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @param $value
     * @return bool
     */
    public function isGreaterThan($value)
    {
        return $this->value > $value;
    }

    /**
     * @param $value
     * @return bool
     */
    public function isLessThan($value)
    {
        return $this->value < $value;
    }

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getMin(): BinaryTreeNodeInterface
    {
        $node = $this;
        while ($node->getLeft() !== null) {
            $node = $this->getLeft();
        }
        return $node;
    }

    /**
     * @return BinaryTreeNodeInterface
     */
    public function getMax(): BinaryTreeNodeInterface
    {
        $node = $this;
        while ($node->getRight() !== null) {
            $node = $node->getRight();
        }
        return $node;
    }

    /**
     * @return bool
     */
    public function isRightNodeOfParent(): bool
    {
        return $this->getParent()->getRight() === $this;
    }

    /**
     * @return bool
     */
    public function isLeftNodeOfParent(): bool
    {
        return $this->getParent()->getLeft() === $this;
    }

    /**
     * @return void
     * @throws BinaryTreeNullValueException
     */
    public function delete(): void
    {
        if ($this->getLeft() && $this->getRight()) {
            $this->handleDeletionWithTwoChildNodes();
        } elseif ($this->getRight()) {
            $this->handleDeletionWithRightOnlyNode();
        } elseif ($this->getLeft()) {
            $this->handleDeletionWithLeftOnlyNode();
        } else {
            $this->handleDeletionWithNullChildNodes();
        }
    }

    /**
     * @return void
     * @throws BinaryTreeNullValueException
     */
    private function handleDeletionWithTwoChildNodes(): void
    {
        $min = $this->getRight()->getMin();
        $this->value = $min->getValue();
        $min->delete();
    }

    /**
     * @return void
     * @throws BinaryTreeNullValueException
     */
    private function handleDeletionWithRightOnlyNode(): void
    {
        if ($this->isLeftNodeOfParent()) {
            $this->getParent()->setLeft(
                $this->getRight()->getValue()
            );
            $this->getRight()->setParent(
                $this->getParent()->getLeft()
            );
        } elseif ($this->isRightNodeOfParent()) {
            $this->getParent()->setRight(
                $this->getRight()
            );
            $this->getRight()->setParent(
                $this->getParent()->getRight()
            );
        }
        $this->setParent(null);
        $this->setRight(null);
    }

    /**
     * @return void
     * @throws BinaryTreeNullValueException
     */
    private function handleDeletionWithLeftOnlyNode(): void
    {
        if ($this->isLeftNodeOfParent()) {
            $this->getParent()->setLeft(
                $this->getLeft()
            );
            $this->getLeft()->setParent(
                $this->getParent()->getLeft()
            );
        } elseif ($this->isRightNodeOfParent()) {
            $this->getParent()->setRight(
                $this->getLeft()
            );
            $this->getLeft()->setParent(
                $this->getParent()->getRight()
            );
        }
        $this->setParent(null);
        $this->setLeft(null);
    }

    /**
     * @return void
     * @throws BinaryTreeNullValueException
     */
    private function handleDeletionWithNullChildNodes(): void
    {
        if ($this->isRightNodeOfParent()) {
            $this->getParent()->setRight(null);
        } elseif ($this->isLeftNodeOfParent()) {
            $this->parent->setLeft(null);
        }
        $this->setParent(null);
    }
}
