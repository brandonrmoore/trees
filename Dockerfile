FROM php:7.4-cli
RUN apt-get update
RUN apt-get -y install git
RUN apt-get install -y libzip-dev && docker-php-ext-install zip
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
